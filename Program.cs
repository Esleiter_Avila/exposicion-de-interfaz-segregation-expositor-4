using System;

namespace segregación_de_interfaz
{
    class Program
    {
        static void Main(string[] args)
        {
            //en esta clase invocamos metodos de avanzado y sencillo
            Multiavanzado avanzado = new Multiavanzado();
            avanzado.Escanear();
            avanzado.Faxear();
            avanzado.Imprimir();
            avanzado.Telefono();

            Console.WriteLine("-------");

            Fax miFax = new Fax();
            miFax.Telefono();
            miFax.Faxear();


            Console.WriteLine("------");

            Multisencillo sencillo = new Multisencillo();
            sencillo.Escanear();
            sencillo.Imprimir();
            //sencillo.telefono();  //Lanzara a la exepción
        }
    }
}
