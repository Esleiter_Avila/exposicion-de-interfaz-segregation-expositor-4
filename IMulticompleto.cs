using System;
using System.Collections.Generic;
using System.Text;
// en esta clase creamos una interfaz  de alto nivel 
//entonces decimos que IMulticompleto va a tener todo lo que tiene IFax y  
//todo aquello que este en la interfaz IMultibasico
namespace segregación_de_interfaz
{
    interface IMulticompleto: IFax, IMultibasico
    {
    }
}
