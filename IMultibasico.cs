using System;
using System.Collections.Generic;
using System.Text;

//multibasico solo va a tener 2 metodos que son imprimir y escanear
namespace segregación_de_interfaz
{
    interface IMultibasico
    {
        void Imprimir();
        void Escanear();
    }
}
