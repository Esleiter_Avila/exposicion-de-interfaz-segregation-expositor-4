using System;
using System.Collections.Generic;
using System.Text;

namespace segregación_de_interfaz
{
    class Multisencillo: IMultibasico
    {
        //solo implementamos los metodos que necesitamos
        public void Imprimir()
        {
            Console.WriteLine("imprimo tu documento");
        }
        public void Escanear()
        {
            Console.WriteLine("escaneo una fotografia");
        }
    }
}
