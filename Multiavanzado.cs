using System;
using System.Collections.Generic;
using System.Text;

//IMulticomoleto tiene 4 metodos que tenemos que implementar
//y aqui llevaremos a cabo la implementacion de aquellos
namespace segregación_de_interfaz
{
    class Multiavanzado: IMulticompleto
    {
        public void Imprimir()
        {
            Console.WriteLine("Imprimo tu reporte");
        }
        public void Escanear()
        {
            Console.WriteLine("estoy esqueneando tu documento");
        }
        public void Telefono()
        {
            Console.WriteLine("te marco a un telefono");
        }
        public void Faxear()
        {
            Console.WriteLine("te envio un fax");
        }
    }
}
