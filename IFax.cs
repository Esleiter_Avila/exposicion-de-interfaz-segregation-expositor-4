using System;
using System.Collections.Generic;
using System.Text;

//esta clase tambien unicamente va a tener 2 metodos  que son telefono y faxear
namespace segregación_de_interfaz
{
    interface IFax
    {
        
		void Telefono();
        void Faxear();
    
    }
}
